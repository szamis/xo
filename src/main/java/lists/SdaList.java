package lists;

public interface SdaList {

    void add(int value);

    int get(int index);

    boolean remove(int index);

    void clear();

    int size();

    boolean contains(int value);

    boolean isEmpty();
}
