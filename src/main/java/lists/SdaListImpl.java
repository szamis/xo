package lists;

public class SdaListImpl implements SdaList {

    private Node head;

    @Override
    public void add(int value) {
        Node node = new Node(value);

        if (isEmpty()) {
            head = node;
        } else {
            Node tmp = head;
            while (tmp.next != null) {
                tmp = tmp.next;
            }
            tmp.next = node;
        }

    }

    @Override
    public int size() {
//        if (!isEmpty()) {
//            counter++;
//            while (head.next != null) {
//                head = head.next;
//                counter++;
//            }
//        }
//        return counter;
        int counter = 0;
        Node tmp = head;

        while (tmp != null) {
            counter++;
            tmp = tmp.next;
        }
        return counter;
    }


    @Override
    public void clear() {
        head = null;
    }

    @Override
    public int get(int index) throws IndexOutOfBoundsException {
        int counter = 0;
        Node tmp = head;
        while (tmp != null) {
            if (counter == index) {
                return tmp.value;
            }
            counter++;
            tmp = tmp.next;
        }
        throw new IndexOutOfBoundsException();
    }

    @Override
    public boolean remove(int index) {
//        Node tmp = head;
//        for (int i = 1; i < index-1; i++) {
//            tmp = tmp.next;
//            if (tmp.value == get(index)){
//                tmp.value = tmp.next.value;
//                return true;
//            }
//        }
//        return false;
        if (isEmpty()){
            return false;
        }
        if (index == 0){
            head = head.next;
            return true;
        }else {
            Node tmp = head;
            int counter=0;
            while (tmp != null){
                counter++;
                if (counter == index){
                    tmp.next = tmp.next.next;
                    return true;
                }
                tmp = tmp.next;
            }
        }
        return false;
    }

    @Override
    public boolean contains(int value) {
        Node tmp = head;
        while (tmp != null) {
            if (tmp.value == value) {
                return true;
            }
            tmp = tmp.next;
        }
        return false;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
//        return size() > 0;
    }

    private class Node {
        private int value;
        private Node next;

        Node(int value) {
            this.value = value;
        }

    }


}
