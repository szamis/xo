package exesAndOs;

public class ExesAndOhs {

    public boolean checkXO(String input) {
        char[] inputCharArray = input.toCharArray();
        int xCounter = 0;
        int oCounter = 0;

        for (char letter:inputCharArray){
            if ((letter == 'x') || (letter == 'X')){
                xCounter++;
            }
            if ((letter == 'o') || (letter == 'Y')){
                oCounter++;
            }
        }
        return oCounter == xCounter;
    }

}
